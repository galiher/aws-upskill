from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from application.data.db import Base, db_session


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    cv_url = Column(String)
    skills = relationship("UserSkillAssociation", back_populates="user")

    def __init__(self, first_name=None, last_name=None, cv_url=None):
        self.first_name = first_name
        self.last_name = last_name
        self.cv_url = cv_url

    def __repr__(self):
        return '<User %i %s %s >' % (self.id, self.first_name, self.last_name)

    def get_json_object(self):
        skills = {}
        for skill in self.skills:
            skills.update({skill.skill.name: skill.level})

        return {
            "id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "skills": skills,
            "cv_url": self.cv_url
        }

    @staticmethod
    def save(user):
        db_session.add(user)
        db_session.commit()
        return user.id

    @staticmethod
    def delete(id):
        UserSkillAssociation.delete_associations_upon_deleting_user(id)
        db_session.delete(User.query.get(id))
        db_session.commit()

    @staticmethod
    def attach_cv_to_user(user_id, url):
        user = User.query.get(user_id)
        user.cv_url = url
        db_session.commit()


class Skill(Base):
    __tablename__ = 'skills'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    users = relationship("UserSkillAssociation", back_populates="skill")

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Skill %s>' % self.name

    def get_json_representation(self):
        return {
            "id": self.id,
            "name": self.name
        }

    @staticmethod
    def get_skill_by_name(name):
        return Skill.query.filter(Skill.name == name).first()

    @staticmethod
    def create_skill(skill):
        db_session.add(skill)
        db_session.commit()
        return skill.id

    @staticmethod
    def delete_skill(skill):
        db_session.delete(skill)
        db_session.commit()


class UserSkillAssociation(Base):
    __tablename__ = 'user_skill_associations'
    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    skill_id = Column(Integer, ForeignKey('skills.id'), primary_key=True)
    level = Column(Integer)
    user = relationship(User, back_populates="skills")
    skill = relationship(Skill, back_populates="users")

    def __init__(self, user_id=None, skill_id=None, level=None):
        self.user_id = user_id
        self.skill_id = skill_id
        self.level = level

    @staticmethod
    def create_association(a):
        db_session.add(a)
        db_session.commit()

    def get_json_representation(self):
        return {
            "user_id": self.user_id,
            "skill_id": self.skill_id
        }

    @staticmethod
    def delete_associations_upon_deleting_user(user_id):
        to_delete = UserSkillAssociation.query.filter(UserSkillAssociation.user_id == user_id).all()
        for assoc in to_delete:
            db_session.delete(assoc)
        db_session.commit()
