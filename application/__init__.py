import os

from flask import Flask

from application.blueprints import cv, user, skill
from application.data.db import db_session


def create_app():
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'db_cv.sqlite'),
    )

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.register_blueprint(user.bp)
    app.register_blueprint(skill.bp)
    app.register_blueprint(cv.bp)


    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db_session.remove()

    return app