import random
import string

import boto3

s3_resource = boto3.resource('s3')
s3_client = boto3.client('s3')


def create_bucket(bucket_name):
    s3_client.create_bucket(Bucket=bucket_name)


def upload_cv(bucket_name, file):
    return s3_resource.Bucket(bucket_name).put_object(Key=file.filename, Body=file)


def get_presigned_url(bucket_name, object_name, expiration=3600):
    response = s3_client.generate_presigned_url('get_object',
                                                Params={
                                                    'Bucket': bucket_name,
                                                    'Key': object_name
                                                },
                                                ExpiresIn=expiration)
    return response


def create_cv(file, first_name, last_name):
    random_string = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(8)])
    bucket_name = (first_name + last_name + random_string).lower()
    create_bucket(bucket_name)
    upload_cv(bucket_name, file)
    url = get_presigned_url(bucket_name, file.filename)
    return url
