from flask import Blueprint, request, Response

from application.helpers.s3_helper import create_cv
from application.blueprints.user import get_user, add_cv_to_user

bp = Blueprint('cv', __name__)


@bp.route("/cv/<int:user_id>", methods=['POST'])
def create_cv_for_user(user_id):
    user = get_user(user_id)
    file = request.files['file']
    url = create_cv(file, user.first_name, user.last_name)
    user.cv_url = url
    add_cv_to_user(user_id, url)
    return Response(status=202, mimetype='application/json')
