import json

from flask import Blueprint, request, Response

from application.data.models import User, UserSkillAssociation
from application.helpers.s3_helper import create_cv
from application.blueprints.skill import get_skill_by_name, create_skill

bp = Blueprint('user', __name__)


@bp.route("/user", methods=['GET'])
def get_users():
    users = []
    for user in User.query.all():
        users.append(user.get_json_object())
    return json.dumps(users)


@bp.route("/user", methods=['POST'])
def add_user():
    first_name = request.form["first_name"]
    last_name = request.form["last_name"]
    u = User(first_name, last_name)
    user_id = User.save(u)
    if 'file' in request.files:
        file = request.files['file']
        url = create_cv(file, first_name, last_name)
        add_cv_to_user(user_id, url)
    s = request.form["skills"]
    skills = json.loads(s)
    for skill_name, skill_level in skills.items():
        skill = get_skill_by_name(skill_name)
        if skill is None:
            skill_id = create_skill(skill_name)
        else:
            skill_id = skill.get_json_representation()["id"]
        UserSkillAssociation.create_association(UserSkillAssociation(user_id, skill_id, skill_level))
    return Response(status=202, mimetype='application/json')


@bp.route("/user/<int:user_id>", methods=['GET'])
def get_user_by_id(user_id):
    return User.query.get(user_id).get_json_object()


@bp.route("/user/<int:user_id>", methods=['DELETE'])
def delete_user(user_id):
    User.delete(user_id)
    return Response(status=204, mimetype='application/json')


def get_user(user_id):
    return User.query.get(user_id)


def add_cv_to_user(user_id, url):
    User.attach_cv_to_user(user_id, url)
