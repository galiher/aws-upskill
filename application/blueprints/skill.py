import json

from flask import Blueprint, request, Response

from application.data.models import Skill

bp = Blueprint('skill', __name__)


@bp.route("/skill/<string:name>", methods=['GET'])
def get_skill(name):
    skill = get_skill_by_name(name)
    if skill is not None:
        return skill.get_json_representation()
    else:
        return Response(response='Skill not found', status=200, mimetype='application/json')


@bp.route("/skill", methods=['GET'])
def get_skills():
    skills = []
    for skill in Skill.query.all():
        skills.append(skill.get_json_representation())
    return json.dumps(skills)


@bp.route("/skill", methods=['POST'])
def add_skill():
    json_data = request.get_json()
    create_skill(json_data["name"])
    return Response(status=202, mimetype='application/json')


def create_skill(name):
    skill = Skill(name)
    return Skill.create_skill(skill)


def get_skill_by_name(name):
    return Skill.get_skill_by_name(name)
